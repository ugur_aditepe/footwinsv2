<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route['default_controller'] = 'site';

$route['admin'] = 'admin/login';
$route['admin/login'] = 'admin/login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
