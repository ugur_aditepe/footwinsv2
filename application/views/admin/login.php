<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			Footwins | Admin Paneli Girişi
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link href="<?php echo base_url();?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/demo/default/media/img/logo/favicon.ico" />
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(<?php echo base_url();?>assets/app/media/img//bg/bg-3.jpg);">
				<div class="m-login__wrapper-1 m-portlet-full-height">
					<div class="m-login__wrapper-1-1">
						<div class="m-login__contanier">
							<div class="m-login__content">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?php echo base_url();?>assets/app/media/img//logos/logo.jpg" style="height:250px;">
									</a>
								</div>
								<div class="m-login__title" style="padding-top:0px;">
									<h3>
										FootWins Admin Portalı
									</h3>
								</div>
								<div class="m-login__desc">
									Admin paneline giriş için bilgilerinizi doldurunuz.
								</div>
							</div>
						</div>
						<div class="m-login__border">
							<div></div>
						</div>
					</div>
				</div>
				<div class="m-login__wrapper-2 m-portlet-full-height">
					<div class="m-login__contanier">
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Hesabın ile Giriş Yap
								</h3>
							</div>
							<form id="loginForm" class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" required type="text" placeholder="Kullanıcı Adı" name="username" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" required type="Password" placeholder="Şifre" name="password">
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left">
										<label class="m-checkbox m-checkbox--focus">
											<input type="checkbox" name="remember" required>
  										 Beni hatırla
											<span></span>
										</label>
									</div>
									<div class="col m--align-right">
										<a href="javascript:;" id="m_login_forget_password" class="m-link">
											Şifremi unuttum ?
										</a>
									</div>
								</div>
								<div class="m-login__form-action">
									<button  type="submit" class="btn btn-brand m-btn m-btn--pill m-btn--custom m-btn--air">
										Giriş Yap
									</button>
								</div>
							</form>
						</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Şifremi unuttum ?
								</h3>
								<div class="m-login__desc">
									Şifreni resetlemek için mail adresiniz :
								</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email Adresiniz" name="email" id="m_email" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<button id="m_login_forget_password_submit" class="btn btn-brand m-btn m-btn--pill m-btn--custom m-btn--air">
										Gönder
									</button>
									<button id="m_login_forget_password_cancel" class="btn btn-outline-brand m-btn m-btn--pill m-btn--custom ">
										İptal
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url();?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/app/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/app/js/pages/admin/login.js" type="text/javascript"></script>
	</body>
</html>
